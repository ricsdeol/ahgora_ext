users = [
	{"name":"Ernane", "registry":"16000076", "password":"064@1985"},
  {"name":"Felipe", "registry":"16000060", "password":"317@1990"},
  {"name":"Jefferson", "registry":"16000057", "password":"397@1992"},
  {"name":"Paty", "registry":"16000043", "password":"088@1990"},
  {"name":"Peps", "registry":"16000055", "password":"228@1992"},
  {"name":"Ricardo", "registry":"16000044", "password":"089@1988"},
  {"name":"Teste", "registry":"160000??", "password":"abc"}
];

get_user = function(registry) {
	return users.filter(
		function(users){
			return users.registry == registry
		}
	);
}

clockin = function (registry){

	var user = get_user(registry);

	var registry = user[0].registry;
	var password = user[0].password;

	$('#account_i').val(registry);
	$('#password_i').val(password);
  $('#botao_entrar').click();
}

build_list = function(){

  var list = document.createElement('div');
  list.id = 'users';
  list.className = 'list-group';

  users.map(
    function(user){
      var a = document.createElement('a');
      a.className = "list-group-item";
      a.text = user.name;
      a.id = user.registry;
      list.appendChild(a);
    }
  );

  return list;
}

create_modal = function(){
  var modal = document.createElement('div');
  modal.className = "modal fade";
  modal.id = "lista-batimentacao";
  modal.setAttribute('role', 'dialog');

  var dialog = document.createElement('div');
  dialog.className = "modal-dialog";

  var m_content = document.createElement('div');
  m_content.className = "modal-content";

  var header = document.createElement('div');
  header.className = "modal-header";

  var close_button = document.createElement('button')
  close_button.type = "button";
  close_button.className = "close";
  close_button.textContent = "x";
  close_button.setAttribute('data-dismiss', 'modal');

  var title = document.createElement('h4');
  title.className = "modal-title";
  title.textContent = "Choose your character player:";
  title.style = "color: black";

  var body = document.createElement('div');
  body.className = 'modal-body';

  var list = build_list();

  header.appendChild(close_button);
  header.appendChild(title);

  body.appendChild(list);

  m_content.appendChild(header);
  m_content.appendChild(body);

  dialog.appendChild(m_content);

  modal.appendChild(dialog);

  var form = $('#onlinePunch');
  form.append(modal);

  $('.list-group-item').click(function(){
    clockin(this.id);
  });

}

initialize = function() {
  create_modal();

  var form = $('#onlinePunch');
  var btn = document.createElement('a');

  btn.className = "btn btn-primary";
  btn.text = "Ver lista";
  btn.id = "abre-lista";
  btn.setAttribute('data-toggle', 'modal');
  btn.setAttribute('data-target', '#lista-batimentacao');

  form.append(btn);
}

$(document).ready(function(){
  initialize();
});
